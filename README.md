# How to get the code
git clone https://gitlab.com/jszweda/tic_tac_toe_websocket.git

# For this code to work u need to have node.js installed
	https://nodejs.org/en/download/

# After u installed node.js
	cd tic_tac_toe_websocket
	./build_script.sh
 The build script file is just used to build the node server and install the modules used

# How to launch
	To launch the website u need to copy the path of the index.html file in the client directory and paste it to your browser