const http = require('http').createServer();

let current_room_list = [];

let current_room_id;

function roomIdUpdate()
{
    let temp;
    if(current_room_list[current_room_list.length-1] === undefined)
    {
        temp = 0;
    }
    else
    {
        temp = current_room_list[current_room_list.length-1]['room_id'] + 1;
    }

    if(isNaN(current_room_id))
        temp = 0;
    
    return temp;
}

current_room_id = roomIdUpdate();


const io = require('socket.io')(3000, {
    cors: {
        origin: '*'
    }
});

io.on('connection', socket => {

    io.emit('receive_room_list', current_room_list);
    socket.on('create_room', () => {
        let temp_obj = {room_id: current_room_id,
                        current_players: 0,
                        players_ready: 0,
                        player_ids: [],
                        game_board: ['','','','','','','','',''],
                        move: 0};
        current_room_list.push(temp_obj);
        current_room_id = roomIdUpdate();
        io.emit('receive_room_list', current_room_list);
    })

    socket.on('joined_game', (id,socket_id) => {
        current_room_list[id]['current_players']++;
        current_room_list[id]['player_ids'].push(socket_id);
        io.emit('receive_room_list', current_room_list);
    })

    socket.on('start_game', (id,socket_id) => {
        current_room_list[id]['players_ready']++;
        if(current_room_list[id]['players_ready'] == 2)
        {
            let starting_player = getRandomInt(2);
            current_room_list[id]['move'] = starting_player;
            io.to(current_room_list[id]['player_ids'][1]).emit('begin', current_room_list[id]['player_ids'][starting_player]);
            io.to(current_room_list[id]['player_ids'][0]).emit('begin', current_room_list[id]['player_ids'][starting_player]);
        }
        else
        {
            io.to(socket_id).emit('standby', 'a');
            if(current_room_list[id]['player_ids'][0] == socket_id)
            {
                io.to(current_room_list[id]['player_ids'][1]).emit('hurry');
            }  
            else
            {
                io.to(current_room_list[id]['player_ids'][0]).emit('hurry');
            }
        }
    })

    socket.on('turn', (field_id, symbol, game_id) => {
        current_room_list[game_id]['game_board'][parseInt(field_id[field_id.length - 1])] = symbol;
        let flag_win = checkWin(current_room_list[game_id]['game_board']);
        if(flag_win == 'no')
        {
            current_room_list[game_id]['move'] = (current_room_list[game_id]['move'] + 1)%2;
            io.to(current_room_list[game_id]['player_ids'][1]).emit('nextTurn', current_room_list[game_id]['game_board'], current_room_list[game_id]['player_ids'][current_room_list[game_id]['move']]);
            io.to(current_room_list[game_id]['player_ids'][0]).emit('nextTurn', current_room_list[game_id]['game_board'], current_room_list[game_id]['player_ids'][current_room_list[game_id]['move']]);
        }
        else
        {
            io.to(current_room_list[game_id]['player_ids'][1]).emit('win', current_room_list[game_id]['game_board'], current_room_list[game_id]['player_ids'][current_room_list[game_id]['move']]);
            io.to(current_room_list[game_id]['player_ids'][0]).emit('win', current_room_list[game_id]['game_board'], current_room_list[game_id]['player_ids'][current_room_list[game_id]['move']]);
        }
        
    })

    socket.on('game_end', game_id => {
        current_room_list[game_id] = {
            room_id: game_id,
            current_players: 0,
            players_ready: 0,
            player_ids: [],
            game_board: ['','','','','','','','',''],
            move: 0
        }
        io.emit('receive_room_list', current_room_list);
    })

    socket.on('disconnect', () => {
        current_room_list.forEach(room => {
            if(room['player_ids'][0] == socket.id)
            {
                socket.to(room['player_ids'][1]).emit('runaway');
            }
            else if(room['player_ids'][1] == socket.id)
            {
                socket.to(room['player_ids'][0]).emit('runaway');
            }
        })
    })

})



function getRandomInt(max){
    return Math.floor(Math.random()*max);
}

function checkWin(game_board)
{
    for(var i = 0 ; i<3 ; i++){
        if(game_board[i] != '' && game_board[i+3] != '' && game_board[i+6] != '' && game_board[i] == game_board[i+3] && game_board[i+6] == game_board[i+3])
        {
            return 'win';
        }
        else if(game_board[i*3] != '' && game_board[(i*3)+1] != '' && game_board[(i*3)+2] != '' && game_board[i*3] == game_board[(i*3)+1] && game_board[(i*3)+2] == game_board[(i*3)+1])
        {
            return 'win';
        }
    }
    if(game_board[0] != '' && game_board[4] != '' && game_board[8] != '' && game_board[0] == game_board[4] && game_board[4] == game_board[8])
    {
        return 'win';
    }
    else if(game_board[2] != '' && game_board[4] != '' && game_board[6] != '' && game_board[2] == game_board[4] && game_board[4] == game_board[6])
    {
        return 'win';
    }
    else
    {
        return 'no';
    }
}


http.listen(5500, () => console.log('listening on port 5500'));