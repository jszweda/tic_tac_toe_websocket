let playing_game = 0;
let playing_game_id;
let socket_id;
let your_symbol;
let timer = 10;
let counter;
const socket = io('http://localhost:3000');

socket.on('connect', ()=> {
    document.getElementById('room_message').textContent = '';
    console.log(socket.id);
    socket_id = socket.id;
})

socket.on('receive_room_list', message => {
    var temp = '';
    if(playing_game === 0)
    {
        for(var i=0;message.length > i; i++)
        {
            temp += `<ul id='room_message_${message[i]['room_id']}' data-players = ${message[i]['current_players']}>Room ${message[i]['room_id']} <button class='join_game' onclick='joinGame(${message[i]['room_id']})' id='game_${message[i]['room_id']}'>join</button> Players ${message[i]['current_players']}/2</ul>`
        }
        document.getElementById('rooms').innerHTML = temp;
    }
    else
    {
        var i = 0;
        while(message[i]['room_id'] !== playing_game_id)
        {
            i++;
        }
        if(message[i]['current_players'] === 1)
        {
            document.getElementById('room_message').textContent = 'Waiting for the second player';
        }
        else
        {
            document.getElementById('room_message').textContent = 'Game is ready to start';
            document.getElementById('room_message').innerHTML += `<br><button id='start_game' onclick='startGame(${message[i]['room_id']})'>Click to start the game </button>`;
        }
    }
    
})

function createRoom(){
    socket.emit('create_room');
}



function joinGame(game_id)
{
    if(document.getElementById(`room_message_${game_id}`).dataset.players < 2)
    {
        document.getElementById('room_menu').style.display = 'none';
        document.getElementById('game_menu').style.display = 'block';
        playing_game = 1;
        playing_game_id = game_id;
        socket.emit('joined_game', game_id, socket_id);
    }
    else
        document.getElementById('error_full').textContent = `Room ${game_id} is full choose a different one`;
}

function startGame(game_id)
{
    socket.emit('start_game', game_id, socket_id);
}

socket.on('begin', starting_id => {
    document.getElementById('room_message').innerHTML = 'The game has started';
    if(starting_id == socket_id)
    {
        your_symbol = 'O';
        turnMy();
    }
    else
    {
        your_symbol = 'X';
        turnYour();
    }
})


socket.on('standby', () => {
    document.getElementById('room_message').innerHTML = 'Waiting for the second player to be ready';
})

socket.on('hurry', () => {
    console.log('hurry_up');
    document.getElementById('room_message').innerHTML += `<br><h5> The second player is waiting for you </h5>`;
})

function turnMy()
{
    document.getElementById('turn').innerHTML = `It's your turn`;
    const fields = document.querySelectorAll('.game_field');
    fields.forEach(field => {
        if(field.dataset.used == 'false')
        {
            field.style.cursor = 'pointer';
            field.addEventListener('click', makeMove);
        }
    })
}

function turnYour()
{
    document.getElementById('turn').innerHTML = `Wait for your opponent to make a move`;
    const fields = document.querySelectorAll('.game_field');
    fields.forEach(field => {
        field.style.cursor = 'default';
        field.removeEventListener('click', makeMove);
    })
}

function makeMove()
{
    console.log(this);
    document.getElementById(this.id).innerHTML = your_symbol;
    document.getElementById(this.id).dataset.used = true;
    
    socket.emit('turn', this.id, your_symbol, playing_game_id);

    
}

socket.on('nextTurn', (game_board, player_id)=> {
    for(var i = 0 ; i<9 ; i++)
    {
        document.getElementById(`field_${i}`).innerHTML = game_board[i];
        if(document.getElementById(`field_${i}`).innerHTML != '')
        {
            document.getElementById(`field_${i}`).dataset.used = 'true';
        }
    }
    if(player_id == socket_id)
    {
        turnMy();
    }
    else
    {
        turnYour();
    }
    
})

socket.on('win', (game_board, player_id)=>{
    for(var i = 0 ; i<9 ; i++)
    {
        document.getElementById(`field_${i}`).innerHTML = game_board[i];
    }
    const fields = document.querySelectorAll('.game_field');
    fields.forEach(field => {
        field.style.cursor = 'default';
        field.removeEventListener('click', makeMove);
    })
    if(player_id == socket_id)
    {
        win();
    }
    else
    {
        lose();
    }
    counter = setInterval(countdown, 1000);
})

socket.on('runaway', () => {
    const fields = document.querySelectorAll('.game_field');
    fields.forEach(field => {
        field.style.cursor = 'default';
        field.removeEventListener('click', makeMove);
    })
    document.getElementById('turn').innerHTML = 'Your opponent ran away (coward)';
    counter = setInterval(countdown, 1000);
})

function win()
{
    document.getElementById('turn').innerHTML = 'YOU WON CONGFERRATULATIONS !!!!';
}
function lose()
{
    document.getElementById('turn').innerHTML = 'YOU lost ;;;;;;(';
}



function countdown()
{
    if(timer == 0)
    {
        clearInterval(counter);
        console.log('returning to the menu');
        lobbyReturn();
    }
    else
    {
        document.getElementById('return_menu').innerHTML = `You will be returned to the lobby in ${timer}`;
        timer--;
    }
    
}

function lobbyReturn()
{
    document.getElementById('room_menu').style.display = 'block';
    document.getElementById('game_menu').style.display = 'none';
    document.getElementById('game_menu').innerHTML = `
    <div id="message_container">
                <h1 id="room_message"><br> </h1>
                <h3 style="display: none;" id="turn_you">It's your turn</h1>
                <h3 style="display: none;" id="turn_opponent">It's your opponents turn</h1>
            </div>
            <div id="game_board">
                <h3 id="turn"></h3>
                <div style="display: table-row">
                    <div class="right bottom game_field" id="field_0" data-used="false"></div>
                    <div class="right bottom game_field" id="field_1" data-used="false"></div>
                    <div class="bottom game_field" id="field_2" data-used="false"></div>
                </div>
                <div style="display: table-row">
                    <div class="right bottom game_field" id="field_3" data-used="false"></div>
                    <div class="right bottom game_field" id="field_4" data-used="false"></div>
                    <div class="bottom game_field" id="field_5" data-used="false"></div>
                </div>
                <div style="display: table-row">
                    <div class="right game_field" id="field_6" data-used="false"></div>
                    <div class="right game_field" id="field_7" data-used="false"></div>
                    <div class="game_field" id="field_8" data-used="false"></div>
                </div>
                <h3 id="return_menu"></h3>
            </div>`;
    socket.emit('game_end', playing_game_id);
    playing_game = 0;
}